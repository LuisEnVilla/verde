var express = require('express');
var router = express.Router();
var request = require('request');

/* GET home page. */

router.get('/delet', function (req, res) {
    req.session.nombre = '';
    req.session.pw = '';
    res.redirect("/");
});

router.get('/registro', function (req, res) {
    if (req.session.nombre && req.session.nombre != ''){
        db.users.findOne({
        $and: [{
            nombre: req.session.nombre
        }, {
            pw: req.session.pw
        }]
    }, function (err, dato) {
        res.render('modificar',{dato:dato});
        });
    }
    else{
        res.render('registro');
    }
});

router.post('/registro', function (req, res) {
   var usuario = new db.users({nombre:req.body.nombre, cuenta:req.body.cuenta, pw:req.body.pw});
    usuario.save();
    res.redirect("/");
});

router.post('/actualizar', function (req, res) {
   db.users.update({nombre : req.session.nombre},{nombre:req.body.nombre, cuenta:req.body.cuenta, pw:req.body.pw},function(req,respuesta){
       res.redirect("/");
   });
});

router.post('/', function (req, res) {
    db.users.findOne({
        $and: [{
            nombre: req.body.nombre
        }, {
            pw: req.body.pass
        }]
    }, function (err, dato) {
        if (!dato) {
            res.render('home', {
                nombre: req.body.nombre
            });
        } else {
            req.session.nombre = dato.nombre;
            req.session.pw = dato.pw;
            db.rutas.find({},function(err, datos){
                res.render('rutas', {nombre: dato.nombre,datos:datos});
                //res.status(200).jsonp(datos);
            });
        }
    });
});


module.exports = router;