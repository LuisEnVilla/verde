module.exports = function(mongoose) {
  var Schema = mongoose.Schema;
  // Objeto modelo de Mongoose
  var SchemaUsers = new Schema({
    nombre        : String,
    pw      : String,
    cuenta : String
  }, {versionKey: false});
  SchemaUsers.set('collection','usuario');
  return mongoose.model('users', SchemaUsers);
}