module.exports = function(mongoose) {
  var Schema = mongoose.Schema;
  // Objeto modelo de Mongoose
  var SchemaUsers = new Schema({
    nombre        : String,
    horario     :   Array,
    paradas     : Array
  }, {versionKey: false});
  SchemaUsers.set('collection','rutas');
  return mongoose.model('rutas', SchemaUsers);
}