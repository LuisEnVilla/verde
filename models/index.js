var mongoose = require('mongoose');
var mongodb_connection_string = 'mongodb://verde:contra123@ds061767.mongolab.com:61767/garzabusdb';

mongoose.connect(mongodb_connection_string,function(err, res) {
	if(!err) console.log('Conectado a BD GarzaBusDB');
});

global.db = {
mongoose: mongoose,
//models
users 	: 	require('./users')(mongoose),
rutas : require('./rutas')(mongoose)
// agregar más modelos aquí en caso de haberlos
};
module.exports = global.db;